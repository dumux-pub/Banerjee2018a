size = 0.01;

xmax = 0.08;
ymax = 0.055;


Point(1) = {0, 0, 0, size};
Point(2) = {xmax, 0, 0, size};
Point(3) = {xmax, ymax, 0, size};
Point(4) = {0, ymax, 0, size};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Transfinite Line{1:4} = 19 Using Progression 1;

Line Loop(5) = {1, 2, 3, 4};

Plane Surface(6) = {5};

Transfinite Surface "*";
Recombine Surface "*";

Physical Surface(7) = {6};


