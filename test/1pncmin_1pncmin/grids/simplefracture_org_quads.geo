numPointsZ = 10;
numPointsX = 10;
size = 0.1;

xmax = 0.08;
ymax = 0.055;
yFrac = 0.0275;
xfrac=0.04;

Point(1) = {0, 0, 0, size};
Point(2) = {xmax, 0, 0, size};
Point(3) = {xmax, yFrac, 0, size};
Point(4) = {0, yFrac, 0, size};
Point(5) = {0, ymax, 0, size};
Point(6) = {xmax, ymax, 0, size};
Point(7)= {xfrac, 0, 0,size};
Point(8) = {xfrac,  yFrac,0,size};
Point(9) = {xfrac,  ymax,0,size};

Line(1) = {4, 5};
Line(2) = {3, 6};
Line(3) = {3, 2};
Line(4) = {4, 1};
Line(5) = {8, 7};
Line(12) = {8, 9};
Transfinite Line{1:5} = numPointsZ Using Progression 1;
Transfinite Line{12} = numPointsZ Using Progression 1;

Line(6) = {9, 5};
Line(7) = {9, 6};
Line(8) = {8, 3};
Line(9) = {7, 2};
Line(10) = {7, 1};
Line(11) = {8, 4};
Transfinite Line{6:11} = numPointsX Using Progression 1;

Line Loop(13) = {1, -6, -12, 11};
Plane Surface(14) = {13};
Line Loop(15) = {12, 7, -2, -8};
Plane Surface(16) = {15};
Line Loop(17) = {5, 9, -3, -8};
Plane Surface(18) = {17};
Line Loop(19) = {5, 10, -4, -11};
Plane Surface(20) = {19};

Transfinite Surface "*";
Recombine Surface "*";

Physical Line(1) = {8, 5, 11};
Physical Surface(2) = {14, 16, 18, 20};
