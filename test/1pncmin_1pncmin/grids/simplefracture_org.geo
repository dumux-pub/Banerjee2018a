size = 0.01;

xmax = 0.08;
ymax = 0.055;
yFrac = 0.0275;
xfrac=0.04;

Point(1) = {0, 0, 0, size};
Point(2) = {xmax, 0, 0, size};
Point(3) = {xmax, yFrac, 0, size};
Point(4) = {0, yFrac, 0, size};
Point(5) = {0, ymax, 0, size};
Point(6) = {xmax, ymax, 0, size};
Point(7)= {xfrac, 0, 0,size};
Point(8)= {xfrac,  yFrac,0,size};

Line(1) = {5, 4};
Line(2) = {4, 1};
Line(3) = {1, 2};
Line(4) = {2, 3};
Line(5) = {3, 6};
Line(6) = {6, 5};
Line(8) = {7,8};
Line (9)= {1, 7};
Line(10) ={8,4};
Line(11)= {7,2};
Line(12) = {3,8};

Line Loop(13) = {1, -10, -12, 5, 6};
Line Loop(14) = {2, 9, 8, 10};
Line Loop (15) ={-8, 11,4, 12};

Plane Surface(16) = {13};
Plane Surface(17) = {14};
Plane Surface(18)= {15};

Physical Surface(19) = {16};
Physical Surface(20) = {17};
Physical Surface(21) = {18};

Physical Line(22) = {8, 10, 12};
//+
