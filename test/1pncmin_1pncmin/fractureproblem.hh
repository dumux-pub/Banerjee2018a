// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-dimensional single-phase fracture model
 */
#ifndef DUMUX_1PNCMIN_FRACTURE_PROBLEM_HH
#define DUMUX_1PNCMIN_FRACTURE_PROBLEM_HH

#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/mixeddimension/subproblemproperties.hh>

#include <dumux/porousmediumflow/1pncmin/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include "simplesteamaircao2h2.hh"
#include "fracturespatialparams.hh"

namespace Dumux
{
//! Forward declaration of the problem class
template <class TypeTag>
class OnePNCMinFractureProblem;

namespace Properties
{
NEW_TYPE_TAG(OnePNCMinIFractureProblem, INHERITS_FROM(OnePNCMin));
NEW_TYPE_TAG(OnePNCMinNIFractureProblem, INHERITS_FROM(OnePNCMinNI));
NEW_TYPE_TAG(OnePNCMinCCIFractureProblem, INHERITS_FROM(CCTpfaModel, OnePNCMinIFractureProblem));
NEW_TYPE_TAG(OnePNCMinCCNIFractureProblem, INHERITS_FROM(CCTpfaModel, OnePNCMinNIFractureProblem));

// Set fluid configuration
SET_TYPE_PROP(OnePNCMinIFractureProblem, FluidSystem, FluidSystems::SteamAirCaO2H2<typename GET_PROP_TYPE(TypeTag, Scalar), false>);
SET_TYPE_PROP(OnePNCMinNIFractureProblem, FluidSystem, FluidSystems::SteamAirCaO2H2<typename GET_PROP_TYPE(TypeTag, Scalar), false>);

// Set the problem property
SET_TYPE_PROP(OnePNCMinIFractureProblem, Problem, OnePNCMinFractureProblem<TypeTag>);
SET_TYPE_PROP(OnePNCMinNIFractureProblem, Problem, OnePNCMinFractureProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(OnePNCMinIFractureProblem, SpatialParams, OnePFractureSpatialParams<TypeTag>);
SET_TYPE_PROP(OnePNCMinNIFractureProblem, SpatialParams, OnePFractureSpatialParams<TypeTag>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(OnePNCMinIFractureProblem, UseMoles, true);
SET_BOOL_PROP(OnePNCMinNIFractureProblem, UseMoles, true);

// Linear solver settings
 SET_TYPE_PROP(OnePNCMinIFractureProblem, LinearSolver, UMFPackBackend<TypeTag>);
 SET_TYPE_PROP(OnePNCMinNIFractureProblem, LinearSolver, UMFPackBackend<TypeTag>);

// Enable gravity
SET_BOOL_PROP(OnePNCMinIFractureProblem, ProblemEnableGravity, false);
SET_BOOL_PROP(OnePNCMinNIFractureProblem, ProblemEnableGravity, false);

// Solution-independent permeability tensor
SET_BOOL_PROP(OnePNCMinCCIFractureProblem, SolutionDependentAdvection, false);
SET_BOOL_PROP(OnePNCMinCCNIFractureProblem, SolutionDependentAdvection, false);
}

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 */
template <class TypeTag>
class OnePNCMinFractureProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);

    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);
    using FluxVariables =  typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // copy some indices for convenience
    enum
    {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        firstMoleFracIdx = Indices::firstMoleFracIdx,
        temperatureIdx=Indices::temperatureIdx,

        // indices of the equations
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::firstTransportEqIdx,
        energyEqIdx = Indices::energyEqIdx,

        // Phase Indices
        phaseIdx = FluidSystem::gPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,
        hPhaseIdx = FluidSystem::hPhaseIdx,

        //Component indices
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,
    };

    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

    static constexpr int numEq = GET_PROP_VALUE(TypeTag, NumEq);
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

public:
    OnePNCMinFractureProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        //initialize fluid system
        FluidSystem::init();

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "_fracture";
        eps_ = 1e-6;
        isCharge_=GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Problem, IsCharge);

        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions"<<std::endl;
        else
            std::cout<<"problem uses mass fractions"<<std::endl;

        // create and initialize output file for flux calculations
        outputFile_.open("balance_1mmap_kratio1_chg_frac_case1.out", std::ios::out); 
        outputFile_ << "Timestep | InFluxEnergy | OutFluxEnergy | InfluxH20 | OutfluxH20 | InfluxAir | OutfluxAir" << std::endl;
    }

    /*!
     * \brief The problem name.
     *        This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; } // 50C

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    {
        //! return the user-specified fracture aperture
        static const Scalar a = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FractureAperture);
        return a;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be used.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (globalPos[0] < eps_)
            values.setAllNeumann();

        if (globalPos[0] > this->bBoxMax()[0] - eps_)
            values.setAllNeumann();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
       // auto values = initialAtPos(globalPos);
       PrimaryVariables priVars(0.0);

        //input parameters
        Scalar pIn;
        Scalar pOut;
        Scalar tIn;
        Scalar tOut;
        Scalar vaporIn;
        Scalar vaporOut;

      // read input parameters
        if (isCharge_ == true){
            pIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureIn);
            pOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureOut);
            tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureIn);
            tOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureOut);
            vaporIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporIn);
            vaporOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporOut);
        }

        else{
            pIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureIn);
            pOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureOut);
            tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureIn);
            tOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureOut);
            vaporIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporInitial);
            vaporOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporOut);
        }

        if(globalPos[0] < eps_)
        {
            priVars[pressureIdx] = pIn;
            priVars[firstMoleFracIdx ]  = vaporIn; 
            priVars[temperatureIdx] = tIn;
        }

        if(globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            priVars[pressureIdx] = pOut;
            priVars[firstMoleFracIdx ] =vaporOut;
            priVars[temperatureIdx] = tOut;
        }

        return priVars;
    }
     /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        control volume.
     */

    PrimaryVariables neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolvars,
                             const SubControlVolumeFace& scvf) const
    {
        Scalar p;

        if (isCharge_==true)
        {
            p = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureOut);
        }
        else
        {
            p = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureOut); 
        }

        const Scalar dirichletPressure = p;

        PrimaryVariables flux(0.0);
        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolvars[scvf.insideScvIdx()];

        if (ipGlobal[1] < eps_ || ipGlobal[1] > this->bBoxMax()[0] - eps_)
            return flux;


        if (ipGlobal[0]<eps_)
        {
            Scalar InFlowAir;
            Scalar InFlowH2O;
            Scalar tIn;

            if (isCharge_==1)
            {
                tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureIn);
                InFlowAir=5;//0.255;//mol/s.m
                InFlowH2O=0.01;
            }

            else
            {
                tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureIn);   
                InFlowAir=2.542;//mol/s.m
                InFlowH2O=2.123;
            }

            //temperature correction
//            Scalar T = elemVolVars[scv].temperature();
//            Scalar deltaH = 0.0; // if temperature at the right border > 573.15 K, cool down 
//                                 //temperature of injected fluid via the enthalpyflux deltaH
//            deltaH = (T-tIn)* 1e10;

           //calculate the enthalpy according to the inlet temperature with a varying pressure
           Scalar hInAir = InFlowAir*FluidSystem::molarMass(firstMoleFracIdx-1)
                           *FluidSystem::componentEnthalpyBorder(volVars.fluidState(), phaseIdx, firstMoleFracIdx-1,tIn);

           Scalar hInH2O = InFlowH2O*FluidSystem::molarMass(firstMoleFracIdx)
                           *FluidSystem::componentEnthalpyBorder(volVars.fluidState(), phaseIdx, firstMoleFracIdx, tIn);
           flux[pressureIdx] = - InFlowAir; //[mol/s] gas inflow of the air component
           flux[firstMoleFracIdx] = - InFlowH2O;//[mol/s] gas inflow of the water component
           flux[temperatureIdx] =  -(hInAir + hInH2O); //[J/s] enthalpy inflow
        }

        // emulate an outflow condition for the component transport on the right side
        if (ipGlobal[0]>this->bBoxMax()[0] - eps_)
        {
            //evaluation of gradient
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = element.geometry().center();
                const Scalar scvCenterPresureSol =volVars.pressure(phaseIdx);
                auto grad = ipGlobal- scvCenter;
                grad /= grad.two_norm2();
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();

            const Scalar K = volVars.permeability();
            const Scalar density =  volVars.molarDensity(phaseIdx);

            // calculate the mole flux
            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0  * K* volVars.mobility(phaseIdx);
            tpfaFlux *=  density;
            if (tpfaFlux < 0.0) tpfaFlux=0.0 ;

            Scalar tpfaMassFlux = gradient * scvf.unitOuterNormal();
            tpfaMassFlux*= -1.0  * K* volVars.mobility(phaseIdx);
            tpfaMassFlux*= volVars.density(phaseIdx);
            if (tpfaMassFlux < 0.0) tpfaMassFlux= 0.0 ;

            flux[conti0EqIdx] = tpfaFlux* (volVars.moleFraction(phaseIdx, (firstMoleFracIdx-1)));
            flux[transportEqIdx] = tpfaFlux  * ( volVars.moleFraction(phaseIdx, firstMoleFracIdx));
            flux[energyEqIdx] = tpfaMassFlux * (FluidSystem::enthalpy(volVars.fluidState(), phaseIdx)); 

            return flux;
        }
    }

    /*!
     * \brief Evaluate the initial value for a control volume (isothermal case)
     */

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);
//         values[pressureIdx] = 1.0e5;
//         values[Indices::temperatureIdx] = temperature();
//         return values;
        if (isCharge_ == true)
        {
            priVars[pressureIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureInitial);
            priVars[firstMoleFracIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporInitial);
            priVars[temperatureIdx] =GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureInitial);
            priVars[CaOIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Fracture, CaOInitial);
            priVars[CaO2H2Idx]   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Fracture, CaO2H2Initial);
        }

        else
        {
        priVars[pressureIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureInitial);
        priVars[firstMoleFracIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporInitial);
        priVars[temperatureIdx] =GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureInitial);
        priVars[CaOIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Fracture, CaOInitial);
        priVars[CaO2H2Idx]   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Fracture, CaO2H2Initial);
        }

        return priVars;
    }


     PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        // we have only sources coming from the bulk domain
        auto sources = couplingManager().evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        sources /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return sources;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    CouplingManager& couplingManager()
    { return *couplingManager_; }

    // write out fluxes to determine the balances in a post processing
    void postTimeStep()
    {
        PrimaryVariables flux(0.0);
        Scalar energyInFlux = 0.0;
        Scalar energyOutFlux = 0.0;
        Scalar H20InFlux =0.0;
        Scalar H20OutFlux =0.0;
        Scalar AirInFlux=0.0;
        Scalar AirOutFlux=0.0;

        for (const auto& element : elements(this->gridView()))
        {
            auto eIdx = this->elementMapper().index(element);


            auto fvGeometry = localView(this->model().globalFvGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(this->model().curGlobalVolVars());
            elemVolVars.bind(element, fvGeometry, this->model().curSol());
            auto elemFluxVarCache = localView(this->model().globalFluxVarsCache());
            elemFluxVarCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto idx = scvf.index();
                const auto& ipGlobal = scvf.ipGlobal();
//                 const auto& area = scvf.area();
//                 const auto& unitOuterNormal = scvf.unitOuterNormal();
//                 auto&& scv = scvf.center();
                const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                FluxVariables fluxVars;
                fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);

                auto upwindRule = [] (const auto& volVars) { return 1.0; };

                if(ipGlobal[0] > 0.08 - eps_)
                {
                    flux = this->neumann(element, fvGeometry, elemVolVars, scvf);
                    energyOutFlux = flux[energyEqIdx];
                    H20OutFlux = flux[transportEqIdx];
                    AirOutFlux = flux[conti0EqIdx];
                }

                if(ipGlobal[0] < 0.0 + eps_ )
                {
                    flux = this->neumann(element, fvGeometry, elemVolVars, scvf);
                    energyInFlux = flux[energyEqIdx];
                    H20InFlux = flux[transportEqIdx];
                    AirInFlux = flux[conti0EqIdx];
                }
            }
        }
         outputFile_ << this->timeManager().timeStepSize() << " | " << energyInFlux << " | " << energyOutFlux << " | " 
        << H20InFlux << " | " << H20OutFlux << " | " 
        << AirInFlux << " | " << AirOutFlux <<std::endl;
    }


private:
    std::string name_;
    Scalar eps_;
    Scalar temperature_;
    std::shared_ptr<CouplingManager> couplingManager_;
    bool isCharge_;

    std::ofstream outputFile_;
    Scalar rightBorderElement_;
    Scalar leftBorderElement_;
};
} //end namespace

#endif
