// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the matrix problem
 */
#ifndef DUMUX_1P_MATRIX_SPATIALPARAMS_HH
#define DUMUX_1P_MATRIX_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/implicit1p.hh>
#include <dumux/porousmediumflow/1pncmin/implicit/indices.hh>

#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>


namespace Dumux
{

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the matrix problem
 */
template<class TypeTag>
class OnePMatrixSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    using ParentType = ImplicitSpatialParamsOneP<TypeTag>;

    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using ElementSolutionVector = typename GET_PROP_TYPE(TypeTag, ElementSolutionVector);
    using PermeabilityLaw=PermeabilityKozenyCarman<TypeTag>;
    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar,dimWorld>;
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    enum {
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        phaseIdx = FluidSystem::gPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,
        hPhaseIdx = FluidSystem::hPhaseIdx,
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents)
    };

    using Tensor = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

public:
    using PermeabilityType = Scalar;
     /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
       OnePMatrixSpatialParams(const Problem& problem, const GridView &gridView)
    : ParentType(problem, gridView)
    {
        //thermal conductivity of CaO
        lambdaSolid_ = 0.4; //[W/(m*K)] Nagel et al [2013b]
        rho_[cPhaseIdx-numPhases] = 1656; //[kg/m^3] density of CaO (see Shao et al. 2014)
        rho_[hPhaseIdx-numPhases] = 2200; //[kg/m^3] density of Ca(OH)_2 (see Shao et al. 2014)
        cp_[cPhaseIdx-numPhases] = 934; //[J/kgK] heat capacity of CaO (see Shao et al. 2014)
        cp_[hPhaseIdx-numPhases] = 1530; //[J/kgK] heat capacity of Ca(OH)_2 (see Shao et al. 2014)
        eps_ = 1e-6;

        permLaw_.init(*this);

        isCharge_=GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,bool,Problem,IsCharge);
    }
     /*!
     * \brief Called by the Problem to initialize the spatial params.
     */
//     void init()
//     {
//         //! Intitialize the parameter laws
//         poroLaw_.init(*this);
//         permLaw_.init(*this);
//     }

    /*!
     *  \brief Define the initial permeability \f$[m^2]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
   Scalar permeability(const Element& element, const SubControlVolume &scv,const ElementSolutionVector& elemSol) const
    {
        //The permeability in the first row is reduced, so that the fluid can distribute here between fracture and matrix.
        if (isfirstcell_(scv.dofPosition()))
        {
            return 5e-11;
        }

       return 5e-12;
    }

     /*!
     *  \brief Define the initial porosity \f$[-]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar initialPorosity(const Element& element, const SubControlVolume &scv) const
    {
        Scalar phi;
        phi = 0.8;
        return phi;
    }

      /*!
     *  \brief Define the minimum porosity \f$[-]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar minPorosity(const Element& element, const SubControlVolume &scv) const
    {
        return 0.8; //intrinsic porosity of CaO2H2 (see Nagel et al. 2014)
    }

      /*!
     *  \brief Define the minimum porosity \f$[-]\f$ after clogging caused by mineralization
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolutionVector& elemSol) const
    { return 0.8; }

    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     */
    Scalar solidHeatCapacity(const Element &element,
                             const SubControlVolume& scv,
                             const ElementSolutionVector& elemSol) const
    {
        auto priVars = elemSol[0];

        Scalar sumPrecipitates = 0.0;
        Scalar effCp = 0.0;

        //calculate the volume average of the two solid phases
        for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < numSPhases; ++solidPhaseIdx)
        {
            sumPrecipitates += priVars[numComponents + solidPhaseIdx];
            effCp += priVars[numComponents + solidPhaseIdx]*cp_[solidPhaseIdx];
        }

        return effCp/sumPrecipitates;
    }


    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     */
    Scalar solidDensity(const Element &element,
                        const SubControlVolume& scv,
                        const ElementSolutionVector& elemSol) const
    {
        auto priVars = elemSol[0];
        Scalar sumPrecipitates = 0.0;
        Scalar effRhoS = 0.0;

        //calculate the volume average of the two solid phases
        for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < numSPhases; ++solidPhaseIdx)
        {
            sumPrecipitates += priVars[numComponents + solidPhaseIdx];
            effRhoS += priVars[numComponents + solidPhaseIdx]*rho_[solidPhaseIdx];
        }
        return effRhoS/sumPrecipitates;
    }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const SubControlVolume& scv,
                                    const ElementSolutionVector& elemSol) const
    { return lambdaSolid_;}

private:
   bool isfirstcell_(const GlobalPosition &globalPos) const
   {return globalPos[0] < 4e-3;}

   Scalar K_; 
   Scalar eps_;
   Scalar lambdaSolid_;
   bool isCharge_;

   PermeabilityLaw permLaw_;
   std::array<Scalar, numSPhases> rho_;
   std::array<Scalar, numSPhases> cp_;


};
} //end namespace

#endif
