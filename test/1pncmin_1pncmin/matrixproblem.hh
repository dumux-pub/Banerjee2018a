// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_1PNCMIN_MATRIX_PROBLEM_HH
#define DUMUX_1PNCMIN_MATRIX_PROBLEM_HH

#include <dumux/mixeddimension/facet/mpfa/properties.hh>
#include <dumux/mixeddimension/subproblemproperties.hh>

#include <dumux/porousmediumflow/1pncmin/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include "simplesteamaircao2h2.hh"
#include "matrixspatialparams.hh"


namespace Dumux
{
template <class TypeTag>
class OnePNCMinMpfaMatrixProblem;

namespace Properties
{
NEW_TYPE_TAG(OnePNCMinIMatrixProblem, INHERITS_FROM(OnePNCMin));
NEW_TYPE_TAG(OnePNCMinNIMatrixProblem, INHERITS_FROM(OnePNCMinNI));
NEW_TYPE_TAG(OnePNCMinICCMpfaMatrixProblem, INHERITS_FROM(FacetCouplingBulkMpfaModel, OnePNCMinIMatrixProblem));
NEW_TYPE_TAG(OnePNCMinNICCMpfaMatrixProblem, INHERITS_FROM(FacetCouplingBulkMpfaModel, OnePNCMinNIMatrixProblem));

// Set fluid configuration
SET_TYPE_PROP(OnePNCMinIMatrixProblem, FluidSystem, FluidSystems::SteamAirCaO2H2<typename GET_PROP_TYPE(TypeTag, Scalar), false>);
SET_TYPE_PROP(OnePNCMinNIMatrixProblem, FluidSystem, FluidSystems::SteamAirCaO2H2<typename GET_PROP_TYPE(TypeTag, Scalar), false>);

// Set the problem property
SET_TYPE_PROP(OnePNCMinIMatrixProblem, Problem, OnePNCMinMpfaMatrixProblem<TypeTag>);
SET_TYPE_PROP(OnePNCMinNIMatrixProblem, Problem, OnePNCMinMpfaMatrixProblem<TypeTag>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(OnePNCMinIMatrixProblem, UseMoles, true);
SET_BOOL_PROP(OnePNCMinNIMatrixProblem, UseMoles, true);

// Set the spatial parameters
SET_TYPE_PROP(OnePNCMinIMatrixProblem, SpatialParams, OnePMatrixSpatialParams<TypeTag>);
SET_TYPE_PROP(OnePNCMinNIMatrixProblem, SpatialParams, OnePMatrixSpatialParams<TypeTag>);

// Linear solver settings
SET_TYPE_PROP(OnePNCMinIMatrixProblem, LinearSolver, UMFPackBackend<TypeTag>);
SET_TYPE_PROP(OnePNCMinNIMatrixProblem, LinearSolver, UMFPackBackend<TypeTag>);

// Enable gravity
SET_BOOL_PROP(OnePNCMinIMatrixProblem, ProblemEnableGravity, false);
SET_BOOL_PROP(OnePNCMinNIMatrixProblem, ProblemEnableGravity, false);

// Solution-independent tensor
SET_BOOL_PROP(OnePNCMinICCMpfaMatrixProblem, SolutionDependentAdvection, false);
SET_BOOL_PROP(OnePNCMinNICCMpfaMatrixProblem, SolutionDependentAdvection, false);

// change mpfa method
//SET_PROP(OnePMatrixProblem, MpfaMethod) { static const MpfaMethods value = MpfaMethods::lMethod; };
}

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model
 */

template <class TypeTag>
class OnePNCMinMpfaMatrixProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using ElementSolutionVector = typename GET_PROP_TYPE(TypeTag, ElementSolutionVector);
    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);
    using FluxVariables =  typename GET_PROP_TYPE(TypeTag, FluxVariables);


    // copy some indices for convenience
    enum
    {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        firstMoleFracIdx = Indices::firstMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx,

        // indices of the equations
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::firstTransportEqIdx,
        energyEqIdx = Indices::energyEqIdx,
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Phase Indices
        phaseIdx = FluidSystem::gPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,
        hPhaseIdx = FluidSystem::hPhaseIdx
    };

    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

    static constexpr int numEq = GET_PROP_VALUE(TypeTag, NumEq);
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;


public:
    OnePNCMinMpfaMatrixProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        //initialize fluid system
        FluidSystem::init();

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "_matrix";
        eps_ = 1e-6;
        isCharge_=GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Problem, IsCharge);

        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions"<<std::endl;
        else
            std::cout<<"problem uses mass fractions"<<std::endl;

        // create and initialize file for flux and storage calculations
        outputFile_.open("balance_1mmap_kratio1_chg_matrix_case1.out", std::ios::out); 

        outputFile_ << "Timestep | InFluxEnergy | OutFluxEnergy | InfluxH20 | OutfluxH20 | InfluxAir | OutfluxAir" << std::endl;
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    {
        return temperature_; 
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto globalPos = scvf.ipGlobal();

        values.setAllNeumann();
       // if (globalPos[0] > this->bBoxMax()[0] - eps_)
         //   values.setAllDirichlet();

        if(globalPos[0] < eps_ )
        {
            values.setAllNeumann();
        }

        if (globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            values.setAllNeumann();
        }

        if (couplingManager().isInteriorBoundary(element, scvf))
            values.setAllNeumann();

        return values;
    }

    /*!
     * \brief Specifies if a given intersection is on an interior boundary
     */
    bool isInteriorBoundary(const Element& element, const Intersection& is) const
    { return couplingManager().isInteriorBoundary(element, is); }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     */
//     PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
//     {
//         //PrimaryVariables values = initialAtPos(globalPos);
//         //return values;
//         //change starts
//            PrimaryVariables priVars(0.0);
//
//         //input parameters
//         Scalar pIn;
//         Scalar pOut;
//         Scalar tIn;
//         Scalar tOut;
//         Scalar vaporIn;
//         Scalar vaporOut;
//
//       // read input parameters
// //         if (isCharge_ == true){
// //             pIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureIn);
// //             pOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureOut);
// //             tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureIn);
// //             tOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureOut);
// //             vaporIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporIn);
// //             vaporOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporOut);
// //
// //         }
// //
// //       else{
// //             pIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureIn);
// //             pOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureOut);
// //             tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureIn);
// //             tOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureOut);
// //             vaporIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporIn);
// //             vaporOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporOut);
// //
// //         }
//
//         if(globalPos[0] < eps_)
//         {
//             priVars[pressureIdx] = pIn;
//             priVars[firstMoleFracIdx ]  = vaporIn; 
//             priVars[temperatureIdx] = tIn;
//         }
// 
//         if(globalPos[0] > this->bBoxMax()[0] - eps_)
//         {
//             priVars[pressureIdx] = pOut;
//             priVars[firstMoleFracIdx ] =vaporOut; //*/0.01; 
//             priVars[temperatureIdx] = tOut;
//         }
//
//         return priVars;
//     }


    PrimaryVariables neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolvars,
                             const SubControlVolumeFace& scvf) const
    {
        Scalar P;

        if (isCharge_==true)
        {
            P= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureOut);
        }
        else
        {
           P= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureOut); 
        }

        const Scalar dirichletPressure = P;

        PrimaryVariables flux(0.0);
        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolvars[scvf.insideScvIdx()];

        if  (ipGlobal[1] < eps_ || ipGlobal[1] > this->bBoxMax()[1] - eps_)
            return flux;

        if (ipGlobal[0]<eps_)
        { 
            Scalar InFlowAir;
            Scalar InFlowH2O;
            Scalar tIn;

            if (isCharge_==1)
            {
                tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureIn);
                InFlowAir=5.0;//0.255;//mol/s.m
                InFlowH2O=0.01;
            }
            else 
            {
                tIn = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureIn);
                InFlowAir=2.542;//mol/s.m
                InFlowH2O=2.123;
            }

            //calculate the enthalpy according to the inlet temperature with a varying pressure
            Scalar hInAir = InFlowAir*FluidSystem::molarMass(firstMoleFracIdx-1)
                           *FluidSystem::componentEnthalpyBorder(volVars.fluidState(), phaseIdx, firstMoleFracIdx-1,tIn);

            Scalar hInH2O = InFlowH2O*FluidSystem::molarMass(firstMoleFracIdx)
                           *FluidSystem::componentEnthalpyBorder(volVars.fluidState(), phaseIdx, firstMoleFracIdx, tIn);

            flux[conti0EqIdx] = - InFlowAir; //[mol/s] gas inflow of the air component
            flux[transportEqIdx] = - InFlowH2O;//[mol/s] gas inflow of the water component
            flux[energyEqIdx] =  -(hInAir + hInH2O); //[J/s] enthalpy inflow

        }

        if (ipGlobal[0]>this->bBoxMax()[0] - eps_)
        {
            // emulate an outflow condition for the component transport on the right side

            //evaluation of gradient
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = element.geometry().center();
                const Scalar scvCenterPresureSol =volVars.pressure(phaseIdx);
                auto grad = ipGlobal- scvCenter;
                grad /= grad.two_norm2();
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();

            const Scalar K = volVars.permeability();
            const Scalar density =  volVars.molarDensity(phaseIdx);

            // calculate the mole flux
            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0  * K* volVars.mobility(phaseIdx);
            tpfaFlux *=  density;
            if (tpfaFlux < 0) tpfaFlux=0;

            // calculate the mass flux
            Scalar tpfaMassFlux =gradient * scvf.unitOuterNormal();
            tpfaMassFlux*= -1.0  * K* volVars.mobility(phaseIdx);
            tpfaMassFlux*= volVars.density(phaseIdx);
            if (tpfaMassFlux<0) tpfaMassFlux=0;


            flux[conti0EqIdx] = tpfaFlux* (volVars.moleFraction(phaseIdx, (firstMoleFracIdx-1)));
            flux[transportEqIdx] = tpfaFlux  * ( volVars.moleFraction(phaseIdx, firstMoleFracIdx));
            flux[energyEqIdx] = tpfaMassFlux * (FluidSystem::enthalpy(volVars.fluidState(), phaseIdx)); 
//         std::cout<< "waterflux [" << ipGlobal[0] << ", " << ipGlobal[1]<<"] = " << flux[transportEqIdx] <<"\n";
            return flux;
    }

    }
    /*!
     * \brief Evaluate the initial value for a control volume (isothermal case)
     */

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        //PrimaryVariables values(0.0);
        //values[pressureIdx] = 1.0e5;
        //values[Indices::temperatureIdx] = temperature();
        //return values;
         PrimaryVariables priVars(0.0);// addn begins

        Scalar pInit;
        Scalar tInit;
        Scalar h2oInit;
        Scalar CaOInit;
        Scalar CaO2H2Init;

        if (isCharge_ == true){
            pInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, PressureInitial);
            tInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, TemperatureInitial);
            h2oInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, VaporInitial);
            CaOInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, CaOInitial);
            CaO2H2Init = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Charge, CaO2H2Initial);
        }

        else {
            pInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, PressureInitial);
            tInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, TemperatureInitial);
            h2oInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, VaporInitial);
            CaOInit = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, CaOInitial);
            CaO2H2Init = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Discharge, CaO2H2Initial);
        }

        priVars[pressureIdx] = pInit;
        priVars[firstMoleFracIdx]   = h2oInit;
        priVars[temperatureIdx] = tInit;
        priVars[CaOIdx] = CaOInit;
        priVars[CaO2H2Idx]   = CaO2H2Init;

        return priVars;

    }
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */

     PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {

        PrimaryVariables source(0.0);
        const auto& volVars = elemVolVars[scv];

        // calculate the equilibrium temperature Teq
        Scalar T= volVars.temperature();
        Scalar Teq = 0;

        Scalar moleFractionVapor = 1e-3;

        if(volVars.moleFraction(phaseIdx, firstMoleFracIdx) > 1e-3)
            moleFractionVapor = volVars.moleFraction(phaseIdx, firstMoleFracIdx);

        if(volVars.moleFraction(phaseIdx, firstMoleFracIdx) >= 1.0) moleFractionVapor = 1;

        Scalar vaporPressure = volVars.pressure(phaseIdx) *moleFractionVapor;
        vaporPressure *= 1.0e-5;
        Scalar pFactor = log(vaporPressure);

        Teq = -12845;
        Teq /= (pFactor - 16.508);        //the equilibrium temperature

        Scalar deltaH = 112e3; // J/mol

        Scalar realSolidDensityAverage = (volVars.precipitateVolumeFraction(hPhaseIdx)*volVars.density(hPhaseIdx)
                                        + volVars.precipitateVolumeFraction(cPhaseIdx)*volVars.density(cPhaseIdx))
                                        / (volVars.precipitateVolumeFraction(hPhaseIdx)
                                        + volVars.precipitateVolumeFraction(cPhaseIdx));

        if(realSolidDensityAverage <= volVars.density(cPhaseIdx))
        {
            realSolidDensityAverage = volVars.density(cPhaseIdx);
        }

        if(realSolidDensityAverage >= volVars.density(hPhaseIdx))
        {
            realSolidDensityAverage = volVars.density(hPhaseIdx);
        }


         //discharge or hydration
        if (T < Teq){

            Scalar massFracH2O_fPhase = volVars.massFraction(phaseIdx, firstMoleFracIdx);
            Scalar krh =0.2;  //0.006

            Scalar rHydration = - massFracH2O_fPhase* (volVars.density(hPhaseIdx)- realSolidDensityAverage)
                                                     * krh * (T-Teq)/ Teq;

            Scalar q =  (1-0.8)*rHydration/(18.01518e-3);

            // make sure not more CaO reacts than present

//          if (-q*timeStepSize_ + volVars.precipitateVolumeFraction(cPhaseIdx)* volVars.molarDensity(cPhaseIdx)< 0 + eps_)
//             {
//                 q = -volVars.precipitateVolumeFraction(cPhaseIdx)* volVars.molarDensity(cPhaseIdx)/timeStepSize_;
//             }


            source[conti0EqIdx+CaO2H2Idx] = q;

            source[conti0EqIdx+CaOIdx] = -q;

            source[conti0EqIdx+firstMoleFracIdx] = -q;

            source[energyEqIdx] = q * deltaH;
        }

        // charge or dehydration
        else if(T > Teq){

            Scalar krd = 0.05; //0.03;
            Scalar rDehydration = (volVars.density(cPhaseIdx)- realSolidDensityAverage)
                                                     * krd * (T-Teq)/ Teq;
            Scalar q = (1-0.8)* rDehydration/(18.01518e-3);                                           


        //make sure not more CaOH2 reacts than present
//            if (q*timeStepSize_ + volVars.precipitateVolumeFraction(hPhaseIdx)* volVars.molarDensity(hPhaseIdx)< 0 + eps_){
//                 q = -volVars.precipitateVolumeFraction(hPhaseIdx)* volVars.molarDensity(hPhaseIdx)/timeStepSize_;
//             }


            source[conti0EqIdx+CaO2H2Idx] = q;

            source[conti0EqIdx+CaOIdx] = -q;

            source[conti0EqIdx+firstMoleFracIdx] = -q;

            source[energyEqIdx] = q * deltaH;
        }

        else
        source = 0.0;

        return source;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }


    CouplingManager& couplingManager()
    { return *couplingManager_; }

    // write out data to evaluate the total balance in postprocessing
    void postTimeStep()
    {
        PrimaryVariables flux(0.0);
        Scalar energyInFlux = 0.0;
        Scalar energyOutFlux = 0.0;
        Scalar H20InFlux =0.0;
        Scalar H20OutFlux =0.0;
        Scalar AirInFlux=0.0;
        Scalar AirOutFlux=0.0;

        for (const auto& element : elements(this->gridView()))
        {
            auto eIdx = this->elementMapper().index(element);

            auto fvGeometry = localView(this->model().globalFvGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(this->model().curGlobalVolVars());
            elemVolVars.bind(element, fvGeometry, this->model().curSol());
            auto elemFluxVarCache = localView(this->model().globalFluxVarsCache());
            elemFluxVarCache.bind(element, fvGeometry, elemVolVars);


            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto idx = scvf.index();
                const auto& ipGlobal = scvf.ipGlobal();
//                 const auto& area = scvf.area();
//                 const auto& unitOuterNormal = scvf.unitOuterNormal();
//                 auto&& scv = scvf.center();
                const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                FluxVariables fluxVars;
                fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);

                auto upwindRule = [] (const auto& volVars) { return 1.0; };

                if(ipGlobal[0] > 0.08 - eps_)
                {
                    flux = this->neumann(element, fvGeometry, elemVolVars, scvf);
                    energyOutFlux += flux[energyEqIdx]; //sum up the fluxes in y direction.
                    H20OutFlux += flux[transportEqIdx];
                    AirOutFlux += flux[conti0EqIdx];
                }

                if(ipGlobal[0] <= 0.0 + eps_ )
                {
                    flux = this->neumann(element, fvGeometry, elemVolVars, scvf);
                    energyInFlux += flux[energyEqIdx];
                    H20InFlux += flux[transportEqIdx];
                    AirInFlux += flux[conti0EqIdx];
                }
            }
        }

        Scalar numCellsY = 2* 18; //divide the fluxes by the number of cells in the y direction

        outputFile_ << this->timeManager().timeStepSize() << " | " << energyInFlux/numCellsY << " | " << energyOutFlux/numCellsY << " | "
        << H20InFlux/numCellsY << " | " << H20OutFlux/numCellsY << " | "
        << AirInFlux/numCellsY << " | " << AirOutFlux/numCellsY <<std::endl;
    }


private:
    std::string name_;
    Scalar eps_;
    Scalar temperature_;
    std::shared_ptr<CouplingManager> couplingManager_;
    bool isCharge_;
    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;

    std::ofstream outputFile_;
    Scalar rightBorderElement_;
    Scalar leftBorderElement_;


};
} //end namespace

#endif
