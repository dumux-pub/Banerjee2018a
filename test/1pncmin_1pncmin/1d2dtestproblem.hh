// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the 1d2d coupled problem:
 *        1d fractures living on the element facets of a 2d matrix
 */
#ifndef DUMUX_1D2D_FACET_TEST_PROBLEM_HH
#define DUMUX_1D2D_FACET_TEST_PROBLEM_HH

#include "fractureproblem.hh"
#include "matrixproblem.hh"

#include <dumux/mixeddimension/facet/properties.hh>
#include <dumux/mixeddimension/problem.hh>
#include <dumux/mixeddimension/facet/gmshdualfacetgridcreator.hh>
#include <dumux/mixeddimension/facet/mpfa/couplingmanager.hh>

namespace Dumux
{
template <class TypeTag>
class OnePNCMinFacetCouplingProblem;

namespace Properties
{
// Type tag of the isothermal and non-isotherman global Problem
NEW_TYPE_TAG(OnePNCMinFacetCouplingProblem, INHERITS_FROM(MixedDimensionFacetCoupling));
NEW_TYPE_TAG(OnePNCMinIFacetCoupling, INHERITS_FROM(OnePNCMinFacetCouplingProblem));
NEW_TYPE_TAG(OnePNCMinNIFacetCoupling, INHERITS_FROM(OnePNCMinFacetCouplingProblem));

// Set the problem property
SET_TYPE_PROP(OnePNCMinFacetCouplingProblem, Problem, Dumux::OnePNCMinFacetCouplingProblem<TypeTag>);

// Set the grid creator
SET_TYPE_PROP(OnePNCMinFacetCouplingProblem, GridCreator, Dumux::GmshDualFacetGridCreator<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(OnePNCMinIFacetCoupling, BulkProblemTypeTag, TTAG(OnePNCMinICCMpfaMatrixProblem));
SET_TYPE_PROP(OnePNCMinIFacetCoupling, LowDimProblemTypeTag, TTAG(OnePNCMinCCIFractureProblem));
SET_TYPE_PROP(OnePNCMinNIFacetCoupling, BulkProblemTypeTag, TTAG(OnePNCMinNICCMpfaMatrixProblem));
SET_TYPE_PROP(OnePNCMinNIFacetCoupling, LowDimProblemTypeTag, TTAG(OnePNCMinCCNIFractureProblem));

// The coupling manager
SET_TYPE_PROP(OnePNCMinFacetCouplingProblem, CouplingManager, CCMpfaFacetCouplingManager<TypeTag>);

// The linear solver to be used
// SET_TYPE_PROP(OnePNCMinFacetCouplingProblem, LinearSolver, ILU0BiCGSTABBackend<TypeTag>);
SET_TYPE_PROP(OnePNCMinFacetCouplingProblem, LinearSolver, UMFPackBackend<TypeTag>);


// The sub-problems need to know the global problem's type tag
SET_TYPE_PROP(OnePNCMinICCMpfaMatrixProblem, GlobalProblemTypeTag, TTAG(OnePNCMinIFacetCoupling));
SET_TYPE_PROP(OnePNCMinCCIFractureProblem, GlobalProblemTypeTag, TTAG(OnePNCMinIFacetCoupling));
SET_TYPE_PROP(OnePNCMinNICCMpfaMatrixProblem, GlobalProblemTypeTag, TTAG(OnePNCMinNIFacetCoupling));
SET_TYPE_PROP(OnePNCMinCCNIFractureProblem, GlobalProblemTypeTag, TTAG(OnePNCMinNIFacetCoupling));

// The subproblems inherit the parameter tree from this problem
SET_PROP(OnePNCMinICCMpfaMatrixProblem, ParameterTree) : GET_PROP(TTAG(OnePNCMinIFacetCoupling), ParameterTree) {};
SET_PROP(OnePNCMinCCIFractureProblem, ParameterTree) : GET_PROP(TTAG(OnePNCMinIFacetCoupling), ParameterTree) {};
SET_PROP(OnePNCMinNICCMpfaMatrixProblem, ParameterTree) : GET_PROP(TTAG(OnePNCMinNIFacetCoupling), ParameterTree) {};
SET_PROP(OnePNCMinCCNIFractureProblem, ParameterTree) : GET_PROP(TTAG(OnePNCMinNIFacetCoupling), ParameterTree) {};

// Set the grids for the two sub-problems
SET_TYPE_PROP(OnePNCMinICCMpfaMatrixProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);
SET_TYPE_PROP(OnePNCMinCCIFractureProblem, Grid, Dune::FoamGrid<1,2>);
SET_TYPE_PROP(OnePNCMinNICCMpfaMatrixProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);
SET_TYPE_PROP(OnePNCMinCCNIFractureProblem, Grid, Dune::FoamGrid<1,2>);

}

template <class TypeTag>
class OnePNCMinFacetCouplingProblem : public MixedDimensionProblem<TypeTag>
{
    using ParentType = MixedDimensionProblem<TypeTag>;

    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using BulkProblemTypeTag = typename GET_PROP_TYPE(TypeTag, BulkProblemTypeTag);
    using LowDimProblemTypeTag = typename GET_PROP_TYPE(TypeTag, LowDimProblemTypeTag);

    using BulkGridView = typename GET_PROP_TYPE(BulkProblemTypeTag, GridView);
    using LowDimGridView = typename GET_PROP_TYPE(LowDimProblemTypeTag, GridView);

public:

    OnePNCMinFacetCouplingProblem(TimeManager &timeManager, const BulkGridView &bulkGridView, const LowDimGridView &lowDimGridView)
    : ParentType(timeManager, bulkGridView, lowDimGridView)
    {}
};

} //end namespace

#endif
