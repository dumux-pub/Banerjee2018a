# dune-common
# releases/2.5 # b7b179b1449024979d488f42483b74d537698e54 # 2017-11-06 18:47:58 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.5
git reset --hard b7b179b1449024979d488f42483b74d537698e54
cd ..

# dune-geometry
# releases/2.5 # ac258d8be39a7f874370bd351945c17c62802a3d # 2017-07-18 12:18:28 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.5
git reset --hard ac258d8be39a7f874370bd351945c17c62802a3d
cd ..

# dune-grid
# releases/2.5 # 8a35ba064c90be4b6c8a921ccdd953d1e0044470 # 2017-08-07 10:29:03 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.5
git reset --hard 8a35ba064c90be4b6c8a921ccdd953d1e0044470
cd ..

# dune-localfunctions
# releases/2.5 # 7ae09a4bea4027ff0afa6b1db12bb43957663c86 # 2017-09-27 10:13:15 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.5
git reset --hard 7ae09a4bea4027ff0afa6b1db12bb43957663c86
cd ..

# dune-istl
# releases/2.5 # 583ad7009ff4de9c6905076ec7ed179c93468957 # 2017-07-18 12:18:59 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.5
git reset --hard 583ad7009ff4de9c6905076ec7ed179c93468957
cd ..

# dune-alugrid
# releases/2.5 # 3a8bcb710d5865ef186a86e01ab9ce59934bb7fd # 2017-06-28 19:52:54 +0100 # dedner
git clone https://gitlab.dune-project.org/extensions/dune-alugrid
cd dune-alugrid
git checkout releases/2.5
git reset --hard 3a8bcb710d5865ef186a86e01ab9ce59934bb7fd
cd ..

# dune-foamgrid
# releases/2.5 # 6d3f1335389e092609c13be7aaf9c085faab4160 # 2017-01-24 14:18:43 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.5
git reset --hard 6d3f1335389e092609c13be7aaf9c085faab4160
cd ..

# dumux
# feature/facet-coupling-1pncmin # 7c0f279a27be0fb1d4487ed2e7d8cf4f5c919a23 # 2017-10-23 12:06:50 +0200 # Gabriele Seitz
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout feature/facet-coupling-1pncmin
git reset --hard 7c0f279a27be0fb1d4487ed2e7d8cf4f5c919a23
patch -p1 < ../patches/dumux_0001-introduce-evalSolution-function.patch
patch -p1 < ../patches/dumux_9999-uncommitted-changes.patch
cd ..

