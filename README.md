Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

I. Banerjee<br>
Modeling Fractures in a CaO/Ca(OH)2 Thermo-chemical Heat Storage Reacture<br>
Master's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 4/2018.


Installation
============

You can build the module just like any other DUNE module. For building and running the executables, please go to the folders
containing the sources listed below. For the basic dependencies see dune-project.org. Note that you have to have the
BOOST library installed for this module.

The easiest way to install this module is to create a new folder and to execute the file
[installBanerjee2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Banerjee2018a/raw/master/installBanerjee2018a.sh)
in this folder.

```bash
mkdir -p Banerjee2018a && cd Banerjee2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/raw/master/installBanerjee2018a.sh
sh ./installBanerjee2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.


Applications
============

The results in the master's thesis are obtained by compiling and running the programs from the sources listed below,
which can be found in test/1pncmin_1pncmin/:<br>

test/1pncmin_1pncmin/test_fracture_1p2cni_1p2cni.cc<br>


Used Versions and Software
==========================

For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[installBanerjee2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/raw/master/installBanerjee2018a.sh).



